#include <unistd.h>
#include <fcntl.h>

int main()
{
    char *archivo = "./archivo";
    char *archivo_copia = "./archivo_copiado";
    char buffer[4096];
    ssize_t tamanio;

    int fichero1 = open(archivo, O_RDONLY);
    int copia_fichero = open(archivo_copia, O_WRONLY | O_CREAT, 0666);

    if (fichero1 >= 0 && copia_fichero >= 0)
    {
        while (tamanio = read(fichero1, buffer, sizeof buffer), tamanio > 0)
        {
            write(copia_fichero, buffer, tamanio);
        }

        close(fichero1);
        close(copia_fichero);
    }

    return 0;
}
